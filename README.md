# Dicom To Mips
Dicom To Mips is a utility gear that convert DICOM file into PNG images by integrating Flywheel dcm2niix gear and Maximum Intensity Projection(MIP) technique to convert dicom file into PNG images.

## Usage

### Inputs
#### Required
* __dicom_input___: Main input file for the Gear. A DICOM archive.

### Configuration

* __threshold_percentile__ (float, default 98.5): The percentile at which to threshold maximum values for MIP.
* __invert_image__ (boolean, default True): Inversion of the output PNG image.
* __verbose__ (boolean, default False): Whether to include verbose output from dcm2niix call.
* __debug__ (boolean, default False): Include debug statements in output.



