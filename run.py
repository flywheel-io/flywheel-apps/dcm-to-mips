#!/usr/bin/python3
"""Gear entrypoint."""
import logging

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_dcm_to_mips import intergration, parser

log = logging.getLogger(__name__)


if __name__ == "__main__":

    with GearToolkitContext() as gear_context:

        # Add required config for dcm2niix gear
        gear_context.config["dcm2niix_verbose"] = gear_context.config["verbose"]
        gear_context.config["anonymize_bids"] = True
        gear_context.config["comment"] = ""
        gear_context.config["compress_images"] = "y"
        gear_context.config["compression_level"] = 6
        gear_context.config["convert_only_series"] = "all"
        gear_context.config["crop"] = False
        gear_context.config["decompress_dicoms"] = False
        gear_context.config["filename"] = "%f"
        gear_context.config["ignore_derived"] = False
        gear_context.config["ignore_errors"] = False
        gear_context.config["lossless_scaling"] = "n"
        gear_context.config["merge2d"] = False
        gear_context.config["output_nrrd"] = False
        gear_context.config["philips_scaling"] = True
        gear_context.config["pydeface"] = False
        gear_context.config["remove_incomplete_volumes"] = False
        gear_context.config["single_file_mode"] = False
        gear_context.config["text_notes_private"] = False
        gear_context.config_json["inputs"]["dcm2niix_input"] = gear_context.config_json[
            "inputs"
        ]["dicom_input"]

        gear_context.init_logging()
        (
            input_filepath,
            input_filename,
            threshold_percentile,
            invert_image,
        ) = parser.parse_config(gear_context)

        intergration.main(
            gear_context,
            gear_context.output_dir,
            threshold_percentile,
            invert_image,
        )
