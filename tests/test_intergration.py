from pathlib import Path
from unittest import mock
from unittest.mock import MagicMock

import pytest
from flywheel_gear_toolkit import GearToolkitContext
from nipype.interfaces.base.support import Bunch, InterfaceResult

import fw_gear_dcm_to_mips
from fw_gear_dcm_to_mips import intergration

OUTPUT_DIR = (Path(__file__).parent / "output").resolve()


@mock.patch("dcm2niix_gear.utils.parse_config.generate_gear_args")
@mock.patch("dcm2niix_gear.dcm2niix.prepare.setup")
@mock.patch("dcm2niix_gear.dcm2niix.dcm2niix_run.convert_directory")
def test_run_dcm_2_niix_gear(dcm2niix_mock, setup_mock, generate_args_mock):

    gc_mock = MagicMock(spec=GearToolkitContext)

    expected_output = InterfaceResult(
        None,
        None,
        None,
        outputs=Bunch(converted_files="/flywheel/v0/work/aliza_siemens_trio.nii.gz"),
    )

    dcm2niix_mock.return_value = expected_output

    output = fw_gear_dcm_to_mips.intergration.run_dcm_2_niix_gear(gc_mock)

    assert output == expected_output


@mock.patch("logging.Logger.info")
@mock.patch("fw_gear_dcm_to_mips.intergration.run_dcm_2_niix_gear")
def test_convert_dcm_to_niix(dcm_2_niix_gear, logging_mock):
    gc_mock = MagicMock(spec=GearToolkitContext)

    expected_converted_files = [
        "/flywheel/v0/work/T1.nii.gz",
        "/flywheel/v0/work/T1_a.nii.gz",
    ]
    expected_output_object = InterfaceResult(
        None,
        None,
        None,
        outputs=Bunch(converted_files=expected_converted_files),
    )

    dcm_2_niix_gear.return_value = expected_output_object

    res = fw_gear_dcm_to_mips.intergration.convert_dcm_to_niix(gc_mock)

    assert all([a == b for a, b in zip(res, expected_converted_files)])

    logging_mock.assert_called_with(
        "Returning converted file(s)... ['/flywheel/v0/work/T1.nii.gz', '/flywheel/v0/work/T1_a.nii.gz']"
    )


@mock.patch("logging.Logger.info")
@mock.patch("fw_gear_nifti_to_mips.converter.main")
@mock.patch("fw_gear_dcm_to_mips.intergration.convert_dcm_to_niix")
def test_main(dcm2niix, nifti_to_mips_mock, logging_mock):
    gc_mock = MagicMock(spec=GearToolkitContext)

    dcm2niix.return_value = [
        "/flywheel/v0/work/T1.nii.gz",
        "/flywheel/v0/work/T1_a.nii.gz",
    ]

    fw_gear_dcm_to_mips.intergration.main(gc_mock, OUTPUT_DIR, 98.5, True)

    logging_mock.assert_called_with("--- Processing nifti-to-mips gear ---")


@mock.patch("fw_gear_dcm_to_mips.intergration.convert_dcm_to_niix")
def test_main_with_no_outputs(convertsion_mock):
    convertsion_mock.return_value = []
    gc_mock = MagicMock(spec=GearToolkitContext)

    with pytest.raises(SystemExit) as e:
        fw_gear_dcm_to_mips.intergration.main(gc_mock, OUTPUT_DIR, 98.5, True)
    assert e.type == SystemExit
    assert e.value.code == 1


@mock.patch("fw_gear_dcm_to_mips.intergration.run_dcm_2_niix_gear")
def test_convert_dcm_to_niix_with_empty_outputs(dcm2niix_mock):
    gc_mock = MagicMock(spec=GearToolkitContext)

    expected_output = InterfaceResult(None, None, None, outputs=None)

    dcm2niix_mock.return_value = expected_output

    converted_files_test = fw_gear_dcm_to_mips.intergration.convert_dcm_to_niix(gc_mock)

    assert converted_files_test is None
