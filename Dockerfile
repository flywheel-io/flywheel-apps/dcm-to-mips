FROM python:3.8-slim as base

# Dev install. git for pip editable install.
RUN apt-get update -qq \
    && apt-get install -y \
    git \
    curl \
    build-essential \
    cmake \
    pkg-config \
    libgdcm-tools \
    libarchive-tools \
    unzip \
    pigz && \ 
    pip install "poetry==1.1.2"


ENV DCMCOMMIT=081c6300d0cf47088f0873cd586c9745498f637a
RUN curl -#L  https://github.com/rordenlab/dcm2niix/archive/$DCMCOMMIT.zip | bsdtar -xf- -C /usr/local
WORKDIR /usr/local/dcm2niix-${DCMCOMMIT}/build
RUN cmake -DUSE_OPENJPEG=ON -DUSE_GIT_PROTOCOL=OFF ../ && \
    make && \
    make install

ENV FLYWHEEL /flywheel/v0
RUN mkdir -p ${FLYWHEEL}
WORKDIR ${FLYWHEEL}

# These layers are the most the most likely to change during dev and
# are kept at the bottom for build time optimization.
COPY manifest.json $FLYWHEEL/manifest.json
COPY run.py $FLYWHEEL/run.py
COPY fw_gear_dcm_to_mips $FLYWHEEL/fw_gear_dcm_to_mips

COPY pyproject.toml poetry.lock ./
RUN poetry install --no-dev

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]
